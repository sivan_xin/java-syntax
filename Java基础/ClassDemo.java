import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/9
 * Java基础——常用类：Number、Math、Arrays、String、StringBuffer、Object
 **/
class Demo {
    public int a = 10;

    public Demo() {
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }
}

public class ClassDemo implements Cloneable {
    //属性引用的对象
    public Demo d = new Demo();

    public static void main(String[] args) throws CloneNotSupportedException {

        ClassDemo c = new ClassDemo();
        ClassDemo clone = c.clone();
        c.d.setA(10);
        //对象的引用类型引用同一个对象。
        System.out.println(c.d == clone.d);
        System.out.println(clone.d.getA());

    }

    @Override
    protected ClassDemo clone() throws CloneNotSupportedException {
        ClassDemo clone = null;
        try {
            clone = (ClassDemo) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        return clone;
    }

    //java.lang.Number.Integer
    @Test
    public void NumberDemo() {
        //一个常量，用于保存 a 可以具有的最大值 2^31-1
        System.out.println(Integer.MAX_VALUE);
        //以数字方式比较两个数的值
        System.out.println(Integer.compare(1, 2));
        //比较两个数中的最大值
        System.out.println(Integer.max(1, 2));
        //parseInt(String s) 将s解析为带符号的十进制整数。
        System.out.println(Integer.parseInt("123"));

        //将字符串（ 第二个参数用来指定进制数） 转换为十进制整数。
        System.out.println(Integer.parseInt("11", 2));

        //toString(int i) 返回表示指定整数的 String对象。
        String s = Integer.toString(123);
        System.out.println(s);

        //valueOf(String s) String类型转int类型 valueOf的底层调用了paesrInt。
        System.out.println(Integer.valueOf("123"));
    }

    //java.lang.Math
    @Test
    public void MathDemo() {
        //比任何其他值都更接近 pi 的值，即圆的周长与其直径的比值
        System.out.println(Math.PI);
        //返回int值的绝对值
        System.out.println(Math.abs(-1));
        //返回第一个参数的值，该值以第二个参数的幂提出。
        System.out.println(Math.pow(2, 10));
        //随机生成一个0-1的数
        System.out.println(Math.random());
        //返回值的正确舍入正平方根
        System.out.println(Math.sqrt(4));
    }

    //java.lang.Arrays
    @Test
    public void ArrayDemo() {
        int[] a = {4, 2, 3, 1};
        //Arrays.asList(1,2,3,4) 数组—>集合
        List<Integer> arrayList = new ArrayList<>();
        arrayList = Arrays.asList(1, 2, 3, 4);

        //Arrays.copyOf(a[],length) 表示复制数组a，length-1个长度。多余位数0补全。
        int[] b = Arrays.copyOf(a, a.length);
        //Arrays.toString(b) 返回指定数组1内容的字符串表示
        System.out.println(Arrays.toString(b));

        //Arrays.sort(a); 将指定的数组按升序排序。
        Arrays.sort(a);

        //Arrays.fill(a,1) 将指定的int值分配给指定的int数组的每个元素。
        Arrays.fill(a, 1);

        //Arrays.equals(a,b) 如果两个指定的int数组彼此 相等 ，则返回 true 。
        Arrays.equals(a, b);

        //Arrays.copyOfRange(a,0,a.length) 将指定数组的指定范围复制到新数组中。
        int[] c = Arrays.copyOfRange(a, 0, a.length);
    }

    //java.lang.String
    @Test
    public void StringTest() {
        String str = "Hello,World";
        /*计算字符串的字符数量*/
        System.out.println(str.length());

        /*分割字符串。""里的作为分界线*/
        //str.split("");
        /*分割后用字符串数组储存。*/
        String[] s = str.split(",");
        //split分割特殊的符号时，应该使用[]将特殊符号括起来。
        System.out.println(s[0]);

        /*截取字符串内容*/
        str.substring(0, 2);  //截取字符串[0，2）的内容。

        /*取出字符串中的字符*/
        str.charAt(0);    //取出第一个字符。

        /*比较字符串大小，str和括号内的比较*/
        str.compareTo("123");    //大于为大于0，小于为小于0。

        /*返回搜索字符或字符串在主串中的位置*/
        str.indexOf("Hello");

        /*搜索重复字符的第二个字符方法*/
        String a = "4456779";
        int loc = a.indexOf('7');
        System.out.println(a.indexOf('7', loc + 1));
        //从指定的索引loc+1开始搜索，是否存在7,可用于有重复字符的情况。

    }

    //java.lng.StringBuffer
    @Test
    public void StringBufferTest() {
        StringBuffer sb = new StringBuffer();
        //将参数的字符串表示形式追加到序列中
        sb.append("123");
        //将数组参数的子数组的字符串表示形式追加到此序列中
        sb.append("456", 0, 3);
        //删除[0，1）的字符
        sb.delete(0, 1);
        //反转字符串
        sb.reverse();
        //返回长度
        sb.length();
        //将参数的字符串表示形式插入到此序列中
        sb.insert(1, "A");

        System.out.println(sb);
    }

    //java.lang.Object
    @Test
    public void ObjectTest() {
        Integer i = 144, j = 144;
        //比较对象地址是否相等
        System.out.println(i == j);
        //比较对象内容是否相等
        System.out.println(i.equals(j));
        //计算hash值
        System.out.println(i.hashCode());

        //反射相关
        //获取运行时类
        System.out.println(i.getClass());

    }
}
