import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/18
 * 常见异常演示 RuntimeException/Exception
 **/
public class ExceptionText {
    //运行时异常
    @Test
    public void text() {
        //java.lang.(Array)IndexOutOfBoundsException
        int[] a = new int[5];
        for (int i = 0; i <= 5; i++) {
            System.out.println(a[i]);
        }
    }

    @Test
    public void text1() {
        //java.lang.NullPointerException
        ExceptionText exceptionEText = null;
        System.out.println(exceptionEText.equals("1"));
    }

    @Test
    public void text2() {
        //java.lang.NumberFormatException
        String str = "123";
        str = "abc";
        int a = Integer.parseInt(str);
    }

    @Test
    public void text3() {
        //ClassCastException
        Object a = new Date();
        String s = (String) a;

    }

    //编译时异常
    @Test
    public void text4() throws IOException {
        //FileNotFoundException
        File file = new File("Hello.text");
        FileInputStream fis = new FileInputStream(file);
        int date = fis.read();
    }

}
