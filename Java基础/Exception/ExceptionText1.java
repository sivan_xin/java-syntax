import org.junit.Test;

import java.io.*;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/18
 * try-catch-finally、throws演示
 **/
public class ExceptionText1 {
    public static void main(String[] args) {
        try {
            method();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            System.out.println("没有输入a[1]");
        }
    }
    //throws 关键字的测试。
    //throws+异常类型 指明方法执行时，可能会抛出的异常类型。
    public static void method()throws Exception{
        int []a=new int[5];
        a[5]=1;
        //满足throws的异常类型时，就抛出异常，异常代码后续的代码，就不再执行。
        System.out.println(a[1]);
    }

    //try-catch语句的说明
    @Test
    public void text(){
        try{
            int a[]=new int[5];
            //出现异常，生成异常对象。
            System.out.println(a[5]);
            //catch：可以捕捉try中抛出的异常对象，并且进行处理。
        }catch (Exception e){
            //e.getMessage() 返回异常的信息
//            System.out.println(e.getMessage());
            //e.printStackTrace() 返回异常的具体信息
            e.printStackTrace();
        }
        System.out.println("try-catch解决了异常，程序继续执行");
    }

    //try如果处理非运行时异常，不加catch编译会不通过。
    //  如果处理运行时异常，可以不加catch。
    @Test
    public void text11(){
        try {
            File file = new File("Hello.txt");
        //    FileInputStream fis = new FileInputStream(file); //提示未处理异常
        }finally {
            System.out.println("NOT RUN");
        }

        try{
            int [] a = new int[5];
            a[5]=10; //运行时异常，可以不加catch
        }finally {
            System.out.println("RUN");
        }
    }


    //finally 一定会被执行的代码,一般用于不可被垃圾回收的资源。
    //比如:数据库链接、输入输出流、网络编程等。
    @Test
    public void text1(){
        FileInputStream fis=null;
        try {
            File file = new File("D:\\java\\demo\\ExceptionText\\src\\Hello.txt");
            //此时没有该文件，fis=null，报出FileNotFoundException
             fis = new FileInputStream(file);
            int date;
            while ((date = fis.read()) != -1) {
                System.out.println((char) date);
            }
        }catch (IOException e){
            System.out.println("1");
            e.printStackTrace();
        }finally {
            //将close写到finally中，表示一定可以关闭流。
            try {
                //如果不写这句话，报出空指针异常。
                if (fis!=null)
                    fis.close();
            }catch (IOException e){
                System.out.println("2");
                e.printStackTrace();
            }
        }

    }

}
