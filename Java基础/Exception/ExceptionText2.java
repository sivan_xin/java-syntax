import com.sun.xml.internal.bind.v2.runtime.reflect.opt.TransducedAccessor_method_Boolean;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/18
 * throw的例子。
 **/
public class ExceptionText2 {
    public static void main(String[] args)  {
        ExceptionText2 exceptionText2=new ExceptionText2();
        try {
            exceptionText2.method(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void method(int id)  throws  Exception{
        if(id<0){
            //生成运行时异常对象，不需要在方法头throws。
            throw new RuntimeException("请输入大于0的数");
        }else {
            //生成编译时异常对象，需要在方法中抛出，并且在main方法中捕获。
            throw new MyException("请输入小于0的数");
        }
    }
}
