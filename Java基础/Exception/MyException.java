/**
 * @Author 不知名网友鑫
 * @Date 2022/4/19
 * 用户自定义异常类
 **/
    //1. 自定义异常类需要继承父类Exception/RunTimeException
public class MyException extends Exception{
    // 2. 自定义异常类需要一个ID作为标识。
    static final long serialVersionUID = -338753124229948L;
    //3. 使用两个构造器——空参
    public MyException(){

    }
    //带参
    public MyException(String msg){
        super(msg);
    }
}
