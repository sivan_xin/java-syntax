/**
 * @Author 不知名网友鑫
 * @Date 2022/6/8
 *Java面向对象——注解
 **/
public class AnnotationTest {

}
//如果注解有成员，需要指明成员的值。
@MyAnnotation(value = "hi")
@MyAnnotation(value = "hi")
class Person{
    public String s ;
    public Person() {

    }

    @Override
    public String toString() {
        return "Person{" +
                "s='" + s + '\'' +
                '}';
    }
}
