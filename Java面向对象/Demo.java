
/**
 * @Author 不知名网友鑫
 * @Date 2022/5/7
 * Java面向对象——类和对象
 **/

package ExerDemo;

//import 可以提供一个路径，使得编译器可以找到某个类。

import org.junit.Test;

import java.util.Scanner;

public class Demo {
    //访问修饰符
    public static int a = 1;
    protected static int b = 1;
    int c = 1;
    private int d = 1;

    public static void main(String[] args) {
        //Scanner的例子
        Scanner in = new Scanner(System.in);

        //静态域与静态方法
        Math.max(a, b);

    }

    //理解类和对象
    @Test
    public void test() {
        //new一个对象，交给dog来管理。
        Dog dog = new Dog(12, "red", 1, "dog");
        //使用对象变量调用方法
        dog.getName();
    }
}

class Dog {
    //成员变量
    private int size;
    private String colour;
    private int age;
    private String name;

    //构造函数（无传入参数）
    public Dog() {

    }

    //构造函数（有传入参数）
    public Dog(int size, String colour, int age, String name) {
        //隐式参数——this，表示调用该方法的对象。
        this.size = size;
        this.colour = colour;
        this.age = age;
        this.name = name;
    }

    //以下为成员函数，表示这个类存在的方法，表示类可以调用的方法。
    void getName() {
        System.out.println(name);
    }

    void getName(String n) {
        this.name = n;
        System.out.println(name);
    }
}

