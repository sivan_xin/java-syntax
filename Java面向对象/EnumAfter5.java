/**
 * @Author 不知名网友鑫
 * @Date 2022/4/19
 * Java面向对象——jdk5.0之后用户自定义枚举类、枚举类中使用方法、实现接口的情况。
 **/
public class EnumAfter5 {
    public static void main(String[] args) {
        Grade1 grade1 = Grade1.FRESHMAN;
        //测试Enum的三个方法：toString、valurOf、value。

        //自定义枚举类继承与Enum类，默认toString为变量名。
        System.out.println(grade1.toString());
        System.out.println();

        //value()方法:返回枚举类的对象数组
        Grade1[] grade = Grade1.values();
        for (int i = 0; i < grade.length; i++) {
            System.out.println(grade[i]);
            grade[i].show();
        }
        System.out.println();

        //valueOf()方法:返回与name同名的对象。
        System.out.println(Grade1.valueOf("FRESHMAN"));
    }
}

interface Info {
    void show();
}

//使用Enum来定义枚举类
enum Grade1 implements Info {
    //1. 先创建对象,注意格式：
    FRESHMAN("大一", "计算机导论") {
        //让枚举类的对象分别实现接口中方法的重写。
        @Override
        public void show() {
            System.out.println("DAYI");
        }

    },  //不同枚举类常量中间使用‘，’来分隔
    SOPHOMORE("大二", "Java程序设计") {
        @Override
        public void show() {
            System.out.println("DAER");
        }
    },
    JUNIOR("大三", "软件工程") {
        @Override
        public void show() {
            System.out.println("DASAN");
        }
    },
    SENIOR("大四", "JavaWeb") {
        @Override
        public void show() {
            System.out.println("DASI");
        }
    };
    private final String GradeDes;  //年级描述
    private final String GradeClass; //年级课程

    //2. 私有化构造器（因为已经确认类的对象的个数，不允许外界调用构造器）
    // 枚举类中默认是private的。
    Grade1(String GradeDes, String GradeClass) {
        this.GradeClass = GradeClass;
        this.GradeDes = GradeDes;
    }

    //4. 其他实现：toString 、访问对象属性······
    public String getGradeDes() {
        return GradeDes;
    }

    public String getGradeClass() {
        return GradeClass;
    }

}

