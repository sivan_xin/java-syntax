/**
 * @Author 不知名网友鑫
 * @Date 2022/4/19
 * Java面向对象——jdk5.0之前用户自定义枚举类（不使用Enum的情况）
 **/
public class EnumBefore5 {
    public static void main(String[] args) {
        System.out.println(Grade.FRESHMAN.toString());
        System.out.println(Grade.JUNIOR.toString());
        System.out.println(Grade.SENIOR.toString());
        System.out.println(Grade.SOPHOMORE.toString());
    }
}

class Grade {
    //1. 定义类的属性，使用private static final修饰
    private final String GradeDes;  //年级描述
    private final String GradeClass; //年级课程

    //2. 私有化构造器（因为已经确认类的对象的个数，不允许外界调用构造器）
    private Grade(String GradeDes, String GradeClass) {
        this.GradeClass = GradeClass;
        this.GradeDes = GradeDes;
    }

    //3.  创建对象
    public static final Grade FRESHMAN = new Grade("大一", "计算机导论");
    public static final Grade SOPHOMORE = new Grade("大二", "Java程序设计");
    public static final Grade JUNIOR = new Grade("大三", "软件工程");
    public static final Grade SENIOR = new Grade("大四", "JavaWeb");

    //4. 其他实现：toString 、访问对象属性······
    public String getGradeDes() {
        return GradeDes;
    }

    public String getGradeClass() {
        return GradeClass;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "GradeDes='" + GradeDes + '\'' +
                ", GradeClass='" + GradeClass + '\'' +
                '}';
    }
}

