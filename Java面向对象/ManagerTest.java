package ExerDemo;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/8
 * Java面向对象——继承、重写&重载、多态
 **/
public class ManagerTest {
    public static void main(String[] args) {
        Employee employee = new Employee("Wang", 1000);
        //多态的使用（对象变量可以有多种形态）
        Employee mag = new Manager("Xin", 1000, 200);
        System.out.println("普通雇员的薪水是：" + employee.getSalary());
        System.out.println("经理的薪水是：" + mag.getSalary());
    }
}

//extends关键字，表示Manager方法继承于Employee。
class Manager extends Employee {
    private double salary;
    //final修饰变量
    private static final int MAX = Integer.MAX_VALUE;

    public Manager() {

    }

    public Manager(String name, double baseSalary, double salary) {
        //使用super关键字，调用父类构造器。
        super(name, baseSalary);
        this.salary = salary;
    }

    //子类重写父类方法
    @Override
    public double getSalary() {
        //使用super关键字，调用父类方法。
        return super.getSalary() + salary;
    }

    public boolean judge() {
        Manager m = new Manager();
        //instanceof的使用
        return m instanceof Employee;
    }
}

class Employee {
    public String name;
    public double baseSalary;

    public Employee() {
    }

    //重载，多用与构造器的重载(Overload)
    public Employee(String name, double baseSalary) {
        this.name = name;
        this.baseSalary = baseSalary;
    }

    public double getSalary() {
        return baseSalary;
    }
}
