package ExerDemo;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/8
 * Java面向对象——继承、重写&重载、多态
 **/
public class StudentText {
    public static void main(String[] args) {
        //父类引用指向子类对象。
        Person p =new Student(12,"Sivan",123);
        System.out.println(p.getName());  //Sivan
        //编译不通过，因为父类中没有此方法。
        // p.getNumber();

        //编译通过，运行通过
        Student s=new Student(12,"Sivan",123);
        System.out.println(s.getNumber()); //123
        p.eat();
        s.eat();
        System.out.println(p.equals(s));  //true
    }
}
//定义接口，接口中的方法都是抽象的
interface Info{
    //接口的修饰符默认都是 public abstract
    void eat();
    void walk();
}

class Student extends Person {
    private int number; //学号
    public Student(int age, String name, int number) {
        super(age, name);
        this.number = number;
    }
    public int getNumber() {
        return number;
    }
    //重写接口方法
    @Override
    public void eat(){
        System.out.println("Student EAT");
    }
}

//类调用接口
class Person implements Info{
    private int age;
    private String name;
    //实现两个构造器，重载的实例
    public Person(){

    }
    public Person(int age,String name){
        this.age=age;
        this.name=name;
    }
    public String  getName(){
        return name;
    }
    //实现接口的功能
    public void eat(){
        System.out.println("EAT");
    }
    public void walk(){
        System.out.println("WALK");
    }
}

