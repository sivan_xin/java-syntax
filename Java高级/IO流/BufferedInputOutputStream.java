import org.junit.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/15
 * 处理流：缓冲流演示
 **/
public class BufferedInputOutputStream {
    //实现图片的复制
    @Test
    public void text1() throws IOException {
        //1. 造文件、造流
//        File file=new File("copy.jpg");
//        File file1=new File("copyy.jpg");
//        FileInputStream fileInputStream = new FileInputStream(file);
//        FileOutputStream fileOutputStream = new FileOutputStream(file1);
//        BufferedInputStream bis=new BufferedInputStream(fileInputStream);
//        BufferedOutputStream bos=new BufferedOutputStream(fileOutputStream);
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(new File("copy.jpg")));
            bos = new BufferedOutputStream(new FileOutputStream(new File("copyy.jpg")));
            //2. 执行复制操作。
            byte[] b = new byte[1024];
            int len;
            while ((len = bis.read(b)) != -1) {
                bos.write(b, 0, len);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            //3. 关闭流
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
