import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/13
 * 文件类的练习 FileDemo
 **/
public class FileExer {
    //1T 利用File构造器，new 一个文件目录file
    @Test
    public void exe1() throws IOException {
        //1)在其中创建多个文件和目录
        //2)编写方法，实现删除file中指定文件的操作
        File file = new File("file");
        File file1 = new File(file, "test1.txt");
        File file2 = new File(file, "test2.txt");
        if (!file.exists()) {
            file.mkdir();
        }
        if (!file1.exists()) {
            file1.createNewFile();
        }
        if (!file2.exists()) {
            file2.createNewFile();
        }
        file2.delete();

        //2T 判断指定目录下是否有后缀名为.jpg的文件，如果有，就输出该文件名称
        String[] str = file.list();
        for (int i = 0; i < str.length; i++) {
            //判断字符串是否以指定后缀结束。
            if (str[i].endsWith(".jpg")) {
                System.out.println(str[i]);
            }
        }

    }

    //3T 遍历指定目录所有文件名称，包括子文件目录中的文件。
    @Test
    public void exe2() {
        //拓展1：并计算指定目录占用空间的大小
        //拓展2：删除指定文件目录及其下的所有文件
//        getFile(new File("C:\\Users\\不知名网友鑫\\Desktop\\Java资料\\Java基础\\1_课件\\第2部分：Java高级编程\\尚硅谷_宋红康_第13章_IO流"));
        System.out.println(getSpace(new File("D:\\java\\demo\\IO\\file")));
    }

    //得到文件路径及所有子目录下的路径
    public void getFile(File file) {
        File[] files = file.listFiles();
        for (File file1 : files) {
            if (file1.isDirectory()) {
                getFile(file1);
            } else {
                System.out.println(file1.getName());
            }
        }
    }

    //得到文件大小
    public int getSpace(File file) {
        File[] files = file.listFiles();
        int size = 0;
        for (File file1 : files) {
            if (file1.isDirectory()) {
                getSpace(file1);
            } else {
                size += file1.length();
            }
        }
        return size;
    }
}
