import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/13
 * File类——创建File类的实例、获取文件信息的方法、获取文件目录的方法、
 * 文件重命名、判断文件状态的方法、创建和删除硬盘中对应的文件或文件目录
 **/
public class FileDemo {
    //1. 创建File类的实例
    @Test
    public void text1() {
        //构造器1->fileDemo(String filePath)
        File file1 = new File("Hello.txt");
        System.out.println(file1);

        //构造器2—>fileDemo(String parentPath,String childPath)
        File file2 = new File("d:\\java", "Hello.txt");
        System.out.println(file2);

        //构造器3->fileDemo(fileDemo parentFile,String childPath)
        File file3 = new File(file2, "Hi.txt");
        System.out.println(file3);
    }

    //2. 关于获取文件信息的方法
    @Test
    public void text2() {
        java.io.File file1 = new java.io.File("Hello.txt");
        //获取文件绝对路径
        System.out.println(file1.getAbsolutePath());

        //获取文件路径
        System.out.println(file1.getPath());

        //获取文件名称
        System.out.println(file1.getName());

        //获取上层文件目录路径
        System.out.println(file1.getParent());

        //获取文件长度
        System.out.println(file1.length());

        //获取最后一次的修改时间
        System.out.println(new Date(file1.lastModified()));

    }

    //3. 关于获取文件目录的方法
    @Test
    public void text3() {
        java.io.File file1 = new java.io.File("D:\\java\\demo\\IO");
        //获取 指定目录下 的所有文件或者文件目录的名称数组
        String[] list = file1.list();
        for (String s : list) {
            System.out.println(s);
        }
        //获取指定目录下的所有文件或者文件目录的File数组
        File[] files = file1.listFiles();
        for (java.io.File f : files) {
            System.out.println(f);
        }

    }

    //4. 文件重命名 保证新的文件名不存在，而旧文件名存在即可。
    @Test
    public void text4() {
        File file1 = new File("Hello.txt");
        File newname = new File("hi.txt");
        file1.renameTo(newname);
    }

    //5. 判断文件状态的方法
    @Test
    public void text5() {
        java.io.File file1 = new java.io.File("Hello.txt");
        //判断是否是文件目录
        System.out.println(file1.isDirectory());

        //判断是否是文件
        System.out.println(file1.isFile());

        //判断是否存在
        System.out.println(file1.exists());

        //判断是否可读
        System.out.println(file1.canRead());

        //判断是否可写
        System.out.println(file1.canWrite());

        //判断是否隐藏
        System.out.println(file1.isHidden());
    }

    //6. 创建和删除硬盘中对应的文件或文件目录
    @Test
    public void text6() throws IOException {
        //1. 创建或删除文件
//        File file1=new File("Hello.txt");
//        //判断文件是否存在
//        if(!file1.exists()){
        //创建文件夹
//            file1.createNewFile();
//        }else {
//            file1.delete();
//        }

        //2. 创建文件夹
        File file2 = new File("demo1.txt");
        file2.mkdir();
        //或
        File file3 = new File(file2, "Hi1.txt");
        file3.createNewFile();

        //创建文件夹。如果上层文件目录不存在，一并创建
        file2.mkdirs();
        //删除文件夹
        file2.delete();
    }

}
