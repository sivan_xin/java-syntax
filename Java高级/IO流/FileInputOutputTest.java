import org.junit.Test;

import java.awt.*;
import java.io.*;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/14
 * FileInputStream、FileOutputStream。
 **/
public class FileInputOutputTest {
    //举例： 实现图片的复制。
    @Test
    public void text() {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            //1. 实例化文件对象
            File file = new File("C:\\Users\\不知名网友鑫\\Desktop\\图片\\GTA-6-GTA-VI-.jpg");
            File file1 = new File("copy.jpg");
            //2. 创建字节输入输出流
            //fis用来从内存读取到程序（输入）。
            fis = new FileInputStream(file);
            //fos用来从程序输出到内存。
            fos = new FileOutputStream(file1);

            //3. 复制操作(用字节接收)
            byte[] b = new byte[1024];
            int len;
            while ((len = fis.read(b)) != -1) {
                fos.write(b, 0, len);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {

            //4. 关闭流
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

}
