import org.junit.Test;

import java.io.*;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/14
 * FileReader、FileWriter。
 **/
public class FileReaderWriterTest {
    //Reader:实现读取Hello文件到内存（程序）中，并且打印所读取的程序
    @Test
    public void Reader() {
        FileReader fr= null;
        try {

            //1. 实例化文件对象。
            File file = new File("pathname.txt");

            //2. 创建文件读取流。
            fr = new FileReader(file);

            //3. 读取文件到程序
            //法 3.1 read() ：每次读取一个字符的文件。如果读到文件末尾，则返回-1。
//        int date;
//        while((date=fr.read())!=-1){
//            System.out.print((char)date);
//        }
           //法 3.2 read(char[]):每次读取一个char数组长度的文件。如果读到文件末尾，则返回-1。
        char c[]=new char[5];
        int len;
        while((len=fr.read(c))!=-1){
            //3.2.1 法一：
            for(int i=0;i<len;i++) {
                System.out.print(c[i]);
            }
        }
        }

            //3.2.2 法二：
//            String s = new String(c, 0, len);
//            System.out.print(s);
//        }
//        }
        catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            //4. 关闭流。
            try {
                if(fr!=null)
                     fr.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }


    }
    @Test
    //从内存写入到磁盘中。
    public void Write()  {
        FileWriter fw= null;
        try {
            //1. 实例化文件对象(不需要实际存在)
            File file=new File("Hello.txt");

            //2. 实例化读取流对象
            //2.1 在文件末尾添加。
            fw = new FileWriter(file,true);
            //2.2 覆盖原有文件。
//        FileWriter fw=new FileWriter(file);

            //3. 给出读取文件。
            fw.write("def456\n");
            fw.write("ghi789\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {

            //4. 关闭流。
            try {
                if(fw != null)
                    fw.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }

}
