/**
 * @Author 不知名网友鑫
 * @Date 2022/4/21
 * 处理流：转换流演示
 **/

import org.junit.Test;

import java.io.*;

/**
 * 处理流：转换流。
 */
public class InputStreamReaderText {
    //实现文件编码的转换而不乱码。
    @Test
    public void text1() throws IOException {
        //1. 造流、造文件
        FileInputStream fis = new FileInputStream("UTF_UTF.txt");
        FileOutputStream fos = new FileOutputStream("UTF_GBK.txt");

        //构造器：第一个参数需要传入一个流，第二个参数需要传入一个编码格式。
        // 字节输入转换为字符输入。
        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
        // 字符输出转换为字节输出。
        OutputStreamWriter osw = new OutputStreamWriter(fos, "gbk");

        //2. 操作
        char[] chars = new char[20];
        int len;
        while ((len = isr.read(chars)) != -1) {
            osw.write(chars, 0, len);
        }

        //3. 关闭流
        isr.close();
        osw.close();
    }
}
