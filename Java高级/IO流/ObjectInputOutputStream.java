import org.junit.Test;

import java.io.*;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/23
 * 对象流ObjectOutputStream、ObjectInputStream。
 **/
public class ObjectInputOutputStream {
    //演示对象序列化的过程。
    @Test
    public void test1() throws IOException {
        //1. 造流，造对象。
        FileOutputStream fos = new FileOutputStream("Object.bat");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        //2. 调用writeObject方法序列化对象。
        oos.writeObject(new Student_ObjectDemo(13, "Sivan"));
        oos.flush();
        //3. 关闭流。
        oos.close();
        fos.close();
    }

    //演示对象反序列化的过程。
    @Test
    public void test2() throws IOException, ClassNotFoundException {
        //1. 造流，造对象
        FileInputStream fis = new FileInputStream("Object.bat");
        ObjectInputStream ois = new ObjectInputStream(fis);
        //2. 调用readObject方法反序列化对象。
        Student_ObjectDemo so = (Student_ObjectDemo) ois.readObject();
        System.out.println(so.getAge());
        //3. 关闭流
        ois.close();
        fis.close();
    }
}
//1. 实现Serializable接口
class Student_ObjectDemo implements Serializable {
    private int age;
    private String name;
    //2. 提供UID
    private static final long serialVersionUID = 794470754667710L;

    //3. 保证当前类的内部成员变量/函数也都是可序列化的。
    public Student_ObjectDemo() {
    }

    public Student_ObjectDemo(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

