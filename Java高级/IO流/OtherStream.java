import org.junit.Test;

import java.io.*;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/21
 * 标准输入输出流：System.in、System.out 字节流。
 * 打印流：PrintStream 、PrintWriter 字符流。
 * 数据流：DataInputStream 、 DataOutputStream 字节流。
 * 随机存取文件流：RandomAccessFile 字节流。
 **/
public class OtherStream {
    //标准输入输出流、打印流
    //1. 不使用Scanner，从键盘读入一行字符串并打印到屏幕。
    @Test
    public void exe1() throws IOException {
        //1.将System.in传入InputStream，把字节流转换成字符流。
        InputStreamReader isr = new InputStreamReader(System.in);
        //2.使用缓冲流BufferReader提供的readLine()方法，读入一行数据。
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        System.out.println(s);
        isr.close();

    }

    //2. 不使用Scanner，从键盘读入一行字符串并打印到文件。
    @Test
    public void exe2() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        //造流、造文件
        FileOutputStream fos = new FileOutputStream("Other.txt");
        PrintStream ps = new PrintStream(fos, true);
        //把标准输出流改为文件，使用setOut方法，传入一个打印输出流。
        if (ps != null) {
            System.setOut(ps);
        }
        String s = br.readLine();
        System.out.println(s);

    }


    //数据流 DataInputStream、DataOutPutStream
    //1. 写入字符串到文件，并且从文件中取出并打印到控制台
    @Test
    public void test() {
        DataOutputStream dos = null;
        try {
            //造流、造文件。
            DataInputStream dis = new DataInputStream(new FileInputStream("data.txt"));
            dos = new DataOutputStream(new FileOutputStream("data.txt"));
            //读入到文件
            dos.writeInt(123);
            dos.flush();
            //读出并打印到控制台
            System.out.println(dis.readInt());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }


    //RandomAccessFile流。
    //1. 实现文件的插入操作。
    @Test
    public void test1() throws IOException {

        RandomAccessFile raf1 = new RandomAccessFile("Hello.txt", "rw");

        //将指针调到要插入的位置。创建StringBuffer来保存插入位置之后的内容。
        raf1.seek(3);
        StringBuffer sb = new StringBuffer((int) new File("Hello.txt").length());
        int len;
        byte b[] = new byte[20];
        //调用read方法，读取文件内容。
        while ((len = raf1.read(b)) != -1) {
            sb.append(new String(b, 0, len));
        }
        //调回指针
        raf1.seek(3);
        //要插入的文件
        raf1.write("456".getBytes());
        //保存的原文件
        raf1.write(sb.toString().getBytes());
        //关闭流
        raf1.close();
    }
}