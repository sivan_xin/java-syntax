import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/3
 * 调用运行时类的指定属性、方法。
 **/
public class ReflectionTest {
    //掌握1：如何获取运行时类的指定属性？
    @Test
    public void test1() throws Exception {
        //0. 源头，获取Class的实例
        Class clazz = Student.class;
        //1. 获取运行时类的对象，才可以进一步对属性进行操作
        Student s = (Student) clazz.newInstance();
        //2. 获取指定属性
        Field num = clazz.getDeclaredField("num");
        //3.保证当前属性可访问
        num.setAccessible(true);
        //4. 设置指定对象的此属性值
        num.set(s,121);

        System.out.println(num.get(s));
    }
    //掌握2：如何获取运行时类的指定方法？
    @Test
    public void test2() throws Exception {
        //0. 源头，获取Class的实例
        Class<Student> clazz = Student.class;
        //1. 获取运行类的对象，对方法进行操作。(如果是静态方法，直接使用Student.class即可)
        Student s = (Student) clazz.newInstance();
        //2. 获取指定方法
        Method show = clazz.getDeclaredMethod("show", int.class,int.class);
        //3. 保证方法可访问
        show.setAccessible(true);
        //4. invoke(): 给方法赋值，invoke的返回值就是方法返回值。
        Object invoke = show.invoke(s, 123,123);
        System.out.println(invoke);
    }
}
class Student extends  Person{
    private int num;
    public String name;

    public Student() {
    }

    private int show(int a,int b){
        System.out.println("私有属性 "+a+b);
        return a+b;
    }

    private Student(int num, String name) {
        this.num = num;
        this.name = name;
    }

    public int getNum() throws Exception {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "num=" + num +
                ", name='" + name + '\'' +
                '}';
    }
}
class Person{
    public String string;
    private int No;
    public Person(String string, int no) {
        this.string = string;
        No = no;
    }

    public Person() {
    }
}