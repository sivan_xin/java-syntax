import org.junit.Test;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/2
 * 获取Class实例的三种方式
 **/
public class getClassTest {

    //获取Class的实例的方式(掌握前三种)
    //Class的实例不仅仅可以是类，也可以是接口、int类型、String、数组等。
    @Test
    public void test3() throws ClassNotFoundException {
        //方式一： 使用类名+.class
        Class clazz1 =Student.class;
        System.out.println(clazz1);
        //方式二： 通过运行时类的对象，调用getclass()
        Student s = new Student();
        Class clazz2 = s.getClass();
        System.out.println(clazz2);
        //方式三（常用）： 调用Class的静态方法 forName(String classPath)
        Class clazz3 = Class.forName("Student");
        System.out.println(clazz3);
        //测试：三个方法调用的clazz是否相同——三者均指向同一个运行时类。
        System.out.println(clazz1==clazz2);
        System.out.println(clazz1==clazz3);
        //方式四：使用类的加载器：ClassLoader(了解)
        ClassLoader classLoader = Reflection.class.getClassLoader();
        Class student = classLoader.loadClass("Student");
        System.out.println(student);

    }
}
