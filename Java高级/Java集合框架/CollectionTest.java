import org.junit.Test;

import java.util.*;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/14
 * Collection的使用、iterator迭代器
 **/
public class CollectionTest {
    public static void main(String[] args) {
        Collection<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(11);

        //删除list中的指定元素
        list.remove(10);

        //判断当前集合中是否含有该元素
        list.contains(11);

        //返回两个集合的交集，并返回给当前集合
        list.retainAll(new ArrayList<>());

        //判断当前集合的大小
        list.size();

        //集合——>数组：toArray()
        Object[] arr = list.toArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        //数组——>集合：Arrays.asList()
        List<String> list1 = Arrays.asList(new String[]{"AA", "BB"});
        System.out.println(list1);


        //iterator():返回Iterator接口的实例，用于遍历集合元素。
        //使用JUnit测试方法测试Iterator。

    }

    //使用迭代器遍历集合元素
    @Test
    public void test1() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new String("Tom"));
        coll.add(false);

        Iterator iterator = coll.iterator();
        //遍历
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        //错误遍历:因为每次调用`iterator()`方法，都会返回一个新的迭代器。
        while (coll.iterator().hasNext()) {
            System.out.println(coll.iterator().next());
        }

    }
}
