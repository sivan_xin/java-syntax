import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/12
 * Collections工具类的测试、List的复制。
 **/
public class CollectionsTest {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(100);
        arrayList.add(10);
        arrayList.add(1);

        //复制一份List
        List dest = Arrays.asList(new Object[arrayList.size()]);
        System.out.println(dest.size());//list.size();
        //（复制到哪，从哪）
        Collections.copy(dest,arrayList);
        System.out.println(dest);


        //反转List中的元素
        Collections.reverse(arrayList);

        // 对List集合进行随机排序
        Collections.shuffle(arrayList);

        //根据元素的自然顺序进行排序。
        Collections.sort(arrayList);

        //根据元素的自然顺序，返回给定集合中的最大元素
        Collections.max(arrayList);

        //返回指定集合中指定元素的出现次数
        Collections.frequency(arrayList,100);
    }
}
