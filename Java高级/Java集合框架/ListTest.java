import java.util.ArrayList;
import java.util.List;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/12
 * Collection——子接口——List接口
 **/
public class ListTest {
    public static void main(String[] args) {
        List<Integer> notes = new ArrayList<>();
        // 增：往容器内加入东西。
        notes.add(123);

        //在index位置插入ele元素
        notes.add(1, 456);

        //删：删除index位置的东西。后面的东西自动向前补位。
        notes.remove(0);
        //删：删除Object这个对象,(删除2这个元素)
        notes.remove(new Integer(2));

        //改：修改第index位。
        notes.set(0, 121);

        //查：得到容器内的第几位。
        notes.get(0);

        //得到容器大小。
        notes.size();
    }
}
