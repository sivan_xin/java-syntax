import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/13
 * Map接口——HashMap常用API、HashMap的遍历方式
 **/
public class MapTest {
    public static void main(String[] args) {

    }

    //Map常用API
    @Test
    public void test1() {
        HashMap<String, Integer> map = new HashMap<>();
        //添加
        map.put("AA", 123);
        map.put("BB", 456);
        map.put("CC", 111);
        //删除
        map.remove("AA");
        //修改
        map.put("BB", 789);
        //查询(返回key对应的val)
        map.get("BB");
        //长度
        map.size();
        //是否包含指定的key
        map.containsKey("BB");
        //是否包含指定的value
        map.containsValue(123);

        //三种遍历
        //第一种，遍历所有的key
        for (String str : map.keySet()) {
            System.out.println(str);
        }
        //第二种，遍历所有的val
        for (Integer integer : map.values()) {
            System.out.println(integer);
        }
        //第三种，遍历所有的Entry
        for (Map.Entry entry : map.entrySet()) {
            System.out.println(entry);
        }
    }
}
