import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/12
 * Map接口——Properties类——读取配置文件jdbc.properties
 **/
public class PropertiesTest {
    public static void main(String[] args) {
        Properties pro = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("Collection_framework\\src\\jdbc.properties");
            pro.load(fis); // 加载流对应的配置文件
            //返回配置文件中key对应的value
            String name = pro.getProperty("name");
            //如果找不到对应的key，返回默认值。
            String password = pro.getProperty("password", "123");

            System.out.println("name=" + name + ",password=" + password);

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
