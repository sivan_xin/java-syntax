import org.junit.Test;

import java.util.*;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/13
 * Collection——子接口——Set接口——HashSet的使用、LinkedHashSet的使用、TreeSet的使用(自然排序/定制排序)
 **/
public class SetTest {
    //HashSet的使用——无序遍历
    @Test
    public void test1() {
        Set set = new HashSet();
        set.add(456);
        set.add(123);
        set.add(123);
        set.add("AA");
        set.add("CC");
        set.add(new User("Tom", 12));
        set.add(new User("Tom", 12));
        set.add(129);

        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    //LinkedHashSet的使用——遍历有序
    @Test
    public void test2() {
        Set set = new LinkedHashSet();
        set.add(456);
        set.add(123);
        set.add(123);
        set.add("AA");
        set.add("CC");

        //这里添加了相同的对象，会先调用hashCode()方法，之后调用equals()方法。
        set.add(new User("Tom", 12));
        set.add(new User("Tom", 12));

        set.add(129);
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    //TreeSet的自然排序
    @Test
    public void test4() {
        TreeSet set = new TreeSet();

        set.add(new User("Tom", 12));
        set.add(new User("Jerry", 32));
        set.add(new User("Jim", 2));
        set.add(new User("Mike", 65));
        set.add(new User("Jack", 33));
        set.add(new User("Jack", 56));

        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }

    //TreeSet的定制排序
    @Test
    public void test3() {
        //按照年龄从小到大排列
        Comparator com = (o1, o2) -> {
            if (o1 instanceof User && o2 instanceof User) {
                User u1 = (User) o1;
                User u2 = (User) o2;
                return Integer.compare(u1.getAge(), u2.getAge());
            } else {
                throw new RuntimeException("输入的数据类型不匹配");
            }
        };
        //构造器传参了，就按照定制排序的方式来。
        TreeSet set = new TreeSet(com);
        set.add(new User("Tom", 12));
        set.add(new User("Jerry", 32));
        set.add(new User("Jim", 2));
        set.add(new User("Mike", 65));
        set.add(new User("Mary", 33));
        set.add(new User("Jack", 33));
        set.add(new User("Jack", 56));


        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}

//实现Comparable接口，TreSet中使用自然排序。
class User implements Comparable {
    private String name;
    private int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        System.out.println("equals()...");
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        System.out.println("hashCode()...");
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //自然排序——按照姓名从大到小排列,然后年龄从小到大排列
//    @Override
//    public int compareTo(Object o) {
//        if(o instanceof User){
//            User user = (User)o;
//            int compare = -this.name.compareTo(user.name);
//            //如果姓名不相同，返回比较的结果。
//            if(compare != 0){
//                return compare;
//            //姓名相同，比较age。
//            }else{
//                return Integer.compare(this.age,user.age);
//            }
//        }else{
//            throw new RuntimeException("输入的类型不匹配");
//        }
//
//    }
    //自然排序——按照年龄从大到小排序
    @Override
    public int compareTo(Object o) {
        User user = (User) o;
        //加-号即可。
        return -Integer.compare(this.age, user.age);
    }
}