package java3;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/5
 * JDK5.0新增方法：使用Callable接口创建多线程
 **/
public class CallableTest {
    public static void main(String[] args) {

        //3. 创建Callable接口实现类的对象
        Callables callables = new Callables();
        //4. 将此接口实现类的对象作为参数传递到FutureTask构造器中。
        FutureTask futureTask = new FutureTask(callables);
        //5. 将FutureTask的对象作为参数传递到Thread类的构造器中，
        //调用start方法。因为FutureTask实现了Runnable接口
        new Thread(futureTask).start();

        // 如果需要获取Callable的返回值等，可以使用方法
        try {
            //调用get方法，可以获取call的返回值。
            Object sum = futureTask.get();
            System.out.println(sum);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
//1. 创建一个实现Callable的实现类
class Callables implements Callable {
    //2. 实现call方法，将此线程需要执行的操作声明在call()中
    @Override
    public Object call() throws Exception {
        int sum=0;
        for (int i = 0; i < 100; i++) {
             if(i%2==0){
                 System.out.println(i);
                 sum+=i;
             }
        }
        return sum;
    }
}
