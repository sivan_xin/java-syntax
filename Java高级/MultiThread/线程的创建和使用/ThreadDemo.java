package java1;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/4
 * Java高级——多线程——多线程的创建和使用——多线程创建方式之一
 * 继承Thread类
 * 创建两个线程，一个用来遍历100以内的偶数，一个用来遍历100以内的奇数。
 **/
public class ThreadDemo {
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        Mythread2 mythread2 = new Mythread2();
        myThread.start();
        mythread2.start();

        //方式2：
//        new Thread(){
//            @Override
//            public void run() {
//                for(int i=0;i<100;i++){
//                    if(i%2==0){
//                        System.out.println(Thread.currentThread().getName()+":" + i);
//                    }
//                }
//            }
//        }.start();
//        new Thread (){
//            @Override
//            public void run() {
//                for(int i=0;i<100;i++){
//                    if(i%2!=0){
//                        System.out.println(Thread.currentThread().getName() + ":" + i);
//                    }
//                }
//            }
//        }.start();

    }
}
class MyThread extends Thread{
    @Override
    public void run() {
        for(int i=0;i<100;i++){
            if(i%2==0){
                System.out.println(Thread.currentThread().getName()+":" + i);
            }
        }
    }
}
class Mythread2 extends Thread{
    @Override
    public void run() {
        for(int i=0;i<100;i++){
            if(i%2!=0){
                System.out.println(Thread.currentThread().getName() + ":" + i);
            }
        }
    }
}
