package java1;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/4
 * Java高级——多线程——多线程的创建和使用——多线程创建方式之二
 * 实现Runnable接口
 * 创建两个线程，都实现打印偶数。
 **/
public class ThreadDemo1 {
    public static void main(String[] args) {
        MThread mThread = new MThread();
        Thread t1 = new Thread(mThread);
        Thread t2 = new Thread(mThread);
        //设置线程名称
        t1.setName("线程1：");
        t2.setName("线程2：");
        t1.start();
        t2.start();
    }
}

class MThread implements Runnable{
    @Override
    public void run() {
        for(int i=0;i<100;i++){
            if(i%2==0){
                System.out.println(Thread.currentThread().getName()+i);
            }
        }
    }
}
