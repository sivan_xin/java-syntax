package java1;

/**
 * 使用Thread的方式解决卖票问题。
 * [存在线程安全问题]
 */

public class ThreadWindow {
    public static void main(String[] args) {
        ThreadTest t1 = new ThreadTest();
        ThreadTest t2 = new ThreadTest();
        ThreadTest t3 = new ThreadTest();
        t1.start();
        t2.start();
        t3.start();
    }
}
class ThreadTest extends Thread{
    //必须声明为static，这样才可以避免卖了三次票的问题。
    private static int ticket = 100;
    @Override
    public void run() {
        while(true){
            if(ticket>0){
                System.out.println(getName()+"剩余票："+ticket);
                ticket--;
            }else{
                break;
            }
        }
    }
}
