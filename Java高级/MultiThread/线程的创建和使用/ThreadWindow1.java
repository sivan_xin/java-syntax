package java1;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/9
 * 使用Runnable的方式解决卖票问题
 * [存在线程安全问题]
 **/
public class ThreadWindow1 {
    public static void main(String[] args) {
        MyThread1 m = new MyThread1();
        Thread t1 = new Thread(m);
        Thread t2 = new Thread(m);
        Thread t3 = new Thread(m);
        t1.setName("窗口一：");
        t2.setName("窗口二：");
        t3.setName("窗口三：");
        t1.start();
        t2.start();
        t3.start();
    }

}
class MyThread1 implements Runnable{
    //这里的ticket不用声明为static，因为只new了一个对象，更适合处理共享数据的情况。
    private int ticket = 100;
    @Override
    public void run() {
        while(ticket>0){
            System.out.println(Thread.currentThread().getName()+ticket);
            ticket--;
        }
    }
}
