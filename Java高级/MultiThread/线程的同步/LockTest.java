package java2;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/5
 * 线程同步的方式之二：使用Lock
 **/
public class LockTest {
    public static void main(String[] args) {
        Locks l1 = new Locks();
        Thread t1 = new Thread(l1);
        Thread t2 = new Thread(l1);
        Thread t3 = new Thread(l1);
        t1.setName("线程1");
        t2.setName("线程2");
        t3.setName("线程3");
        t1.start();
        t2.start();
        t3.start();
    }
}
class Locks implements Runnable{
    //操作共享数据，使用同步解决操作共享数据的问题。
    private int ticket = 100;
    //1. 创建Lock对象
    private ReentrantLock lock = new ReentrantLock(true);   //传参判断是否公平——按照线程创建顺序获取CPU资源
    @Override
    public void run() {
        try{
            //2. 代表加锁，以下的代码均为同步代码。
            lock.lock();
           while(ticket>0){
                System.out.println(Thread.currentThread().getName()+":"+ticket--);
            }
        }finally {
            //3. 代表解锁，需要手动操作。
            lock.unlock();
        }
    }
}
