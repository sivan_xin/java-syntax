package java2;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/5
 * Runnable方式实现多线程，使用同步方法来解决线程安全问题。
 **/
public class ThreadSafeTest2 {
    public static void main(String[] args) {
        Windows2 w1 = new Windows2();
        Thread t1 = new Thread(w1);
        Thread t2 = new Thread(w1);
        Thread t3 = new Thread(w1);
        t1.start();
        t2.start();
        t3.start();
    }
}
class Windows2 implements Runnable{
    private int ticket = 100;
    @Override
    public void run() {
        while(true){
            show();
            if(ticket==0){
                break;
            }
        }
    }
    //非静态的同步方法，同步监视器为this
    private synchronized void show(){
        if (ticket > 0) {
            System.out.println(Thread.currentThread().getName() + ":" + ticket);
            ticket--;
        }
    }
}
