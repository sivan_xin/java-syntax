package java2;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/5
 * Thread方式实现多线程，使用同步方法来解决线程安全问题。
 **/
public class ThreadSafeTest3 {
    public static void main(String[] args) {
        Windows3 w1 = new Windows3();
        Windows3 w2 = new Windows3();
        Windows3 w3 = new Windows3();
        w1.start();
        w2.start();
        w3.start();
    }
}
class Windows3 extends Thread{
    private static int ticket = 100;
    @Override
    public void run() {
        while(true){
            show();
            if(ticket==0){
                break;
            }
        }
    }
    //应该为静态的同步方法，同步监视器为当前类。
    private static synchronized void show(){
        if (ticket > 0) {
            System.out.println(Thread.currentThread().getName() + ":" + ticket);
            ticket--;
        }
    }
}