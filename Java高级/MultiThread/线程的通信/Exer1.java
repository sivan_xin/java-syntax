package exer;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/5
 *  练习一：使用两个线程打印1-100，线程1，线程2交替打印。
 **/
public class Exer1 {
    public static void main(String[] args) {
        ExThread e1= new ExThread();
        ExThread e2= new ExThread();
        e1.start();
        e2.start();
    }
}
class ExThread extends Thread{
    private static int number=1;
    private static Object obj = new Object();
    @Override
    public void run() {
        while(true) {
            synchronized (obj) {
                // 释放线程
                obj.notify();
                if (number <= 100) {
                    System.out.println(getName() + ":" + number);
                    number++;
                    try {
                        //使线程进入等待状态。
                        obj.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    break;
                }
            }
        }

    }
}
