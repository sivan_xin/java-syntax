import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author 不知名网友鑫
 * @Date 2022/4/23
 * 使用InetAddress类创建IP地址的对象。
 * InetAddress类的方法。
 **/
public class InetAddressTest {
    public static void main(String[] args) throws UnknownHostException {
        //创建InetAddress的对象，就是表示一个IP地址。
        InetAddress inet1 = InetAddress.getByName("14.215.177.38");
        System.out.println(inet1);

        InetAddress inet2 = InetAddress.getByName("www.baidu.com");
        System.out.println(inet2);

        //获取本地ip地址
        InetAddress inet4 = InetAddress.getLocalHost();
        System.out.println(inet4);

        //getHostName()：获取域名
        System.out.println(inet2.getHostName());

        //getHostAddress()：获取IP地址
        System.out.println(inet2.getHostAddress());
    }
}
