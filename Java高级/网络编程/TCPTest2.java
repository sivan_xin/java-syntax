import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/21
 * TCP网络编程练习二:客户端向服务端发送一个图片，并且服务端保存图片到本地。
 **/
public class TCPTest2 {

    @Test
    public void client() throws IOException {
        //1. 创建InetAddress对象和Socket对象，指明服务器端(现在是本地)IP地址和端口号。
        InetAddress inet = InetAddress.getLocalHost();
        Socket socket = new Socket(inet, 13333);
        //2. 创建输入流对象。输入流——将本地图片读取到程序中。
        FileInputStream fis = new FileInputStream("GTA-6.jpg");
        //3. 使用Socket对象获取输出流。输出流——将图片从程序输出到服务端。
        FileOutputStream fos = (FileOutputStream) socket.getOutputStream();
        byte[] b = new byte[1024];
        int len;
        while ((len = fis.read(b)) != -1) {
            fos.write(b, 0, len);
        }
        //4. 关闭流
        fos.close();
        fis.close();
        socket.close();

    }

    @Test
    public void server() throws IOException {
        //1. 创建服务端ServerSocket对象，指明自己端口号。
        ServerSocket serverSocket = new ServerSocket(13333);
        //2. 创建Socket对象，ServerSocket对象调用accept()方法接受来自客户端的socket。
        Socket socket = serverSocket.accept();
        //3. 使用socket对象获取输入流。输入流——将网络中的图片读取到程序。
        FileInputStream fis = (FileInputStream) socket.getInputStream();
        //3. 创建输出流。输出流：将图片从程序输出到内存。
        FileOutputStream fos = new FileOutputStream("GTA.jpg");
        byte[] b = new byte[1024];
        int len;
        while ((len = fis.read(b)) != -1) {
            fos.write(b, 0, len);
        }
        //4. 关闭流。
        fos.close();
        fis.close();
        socket.close();
        serverSocket.close();
    }
}
