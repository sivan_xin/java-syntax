import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/21
 * TCP网络编程练习三：从客户端发送文件给服务端，服务端保存到本地。并返回“发送成功”给客户端。
 * 在Test1的基础上改进。
 **/
public class TCPTest3 {
    @Test
    public void client() throws IOException {
        //1.创建InetAddress对象和Socket对象，指明服务器端(现在是本地)IP地址和端口号。
        InetAddress inet = InetAddress.getByName("127.0.0.1");
        Socket socket = new Socket(inet, 13333);
        //2. 使用socket对象获取输出流。（输出流：将文字从程序输出到网络）
        FileOutputStream fos = (FileOutputStream) socket.getOutputStream();
        fos.write("Hello".getBytes());

        //发送完要关闭输出流，服务器端还要使用。
        socket.shutdownOutput();

        FileInputStream fis1 = (FileInputStream) socket.getInputStream();
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        byte [] bytes = new byte[5];
        int len;
        while ((len=fis1.read(bytes))!=-1){
            bas.write(bytes,0,len);
        }
        System.out.println(bas);

        //3. 关闭流
        fos.close();
        socket.close();
    }
    @Test
    public void server() throws IOException {
        //1. 创建服务端ServerSocket对象，指明自己端口号。
        ServerSocket serverSocket = new ServerSocket(13333);
        //2. 创建Socket对象，ServerSocket对象调用accept()方法接受来自客户端的socket。
        Socket socket = serverSocket.accept();
        //3. 使用Socket对象获取输入流。（输入流：将网络中的信息读取到程序。）
        FileInputStream fis = (FileInputStream) socket.getInputStream();
        //4. 创建输出流。（输出流：进一步将程序中的信息写入到内存中。）
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        byte[] b = new byte[5];
        int len;
        while ((len = fis.read(b)) != -1) {
            bas.write(b, 0, len);
        }
        System.out.println(bas.toString());
        System.out.println("收到" + socket.getInetAddress().getHostAddress() + "的数据");

        OutputStream os = socket.getOutputStream();
        os.write("发送成功".getBytes());

        //5. 关闭流
        bas.close();
        fis.close();
        socket.close();
        serverSocket.close();
    }
}
