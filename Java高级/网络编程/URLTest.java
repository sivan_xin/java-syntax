import java.net.MalformedURLException;
import java.net.URL;

/**
 * @Author 不知名网友鑫
 * @Date 2022/6/22
 * URL类的方法。
 **/
public class URLTest {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("https://www.canva.cn/photos/MAE2O2d8u_o/");
//            public String getProtocol(  )     获取该URL的协议名
        System.out.println(url.getProtocol());
//            public String getHost(  )           获取该URL的主机名
        System.out.println(url.getHost());
//            public String getPort(  )            获取该URL的端口号
        System.out.println(url.getPort());
//            public String getPath(  )           获取该URL的文件路径
        System.out.println(url.getPath());
//            public String getFile(  )             获取该URL的文件名
        System.out.println(url.getFile());
//            public String getQuery(   )        获取该URL的查询名
        System.out.println(url.getQuery());
    }
}
