import javax.net.ssl.HttpsURLConnection;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @Author 不知名网友鑫
 * @Date 2022/5/2
 * 使用URL下载图片资源到本地
 **/
public class URLTest1 {
    public static void main(String[] args) throws IOException {
        //1. 创建URL对象，指定要下载的图片资源
        URL url = new URL("http://www.pp3.cn/uploads/20120418lw/13.jpg");
        // 2. 调用openConnection方法
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        // 3. 调用connect方法
        urlConnection.connect();
        //4. 造流，输入输出流。
        InputStream inputStream = urlConnection.getInputStream();
        FileOutputStream fos = new FileOutputStream("Internet\\12.png");
        byte b[] = new byte[1024];
        int len;
        while((len=inputStream.read(b))!=-1){
            fos.write(b,0,len);
        }
        //5. 关闭流
        fos.close();
        inputStream.close();
        urlConnection.disconnect();
    }
}
