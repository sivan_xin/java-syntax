CSDN博客名：Sivan_Xin 博客主页：[CSDN-Sivan_Xin](https://blog.csdn.net/weixin_62633072?spm=1000.2115.3001.5343),
另附Java8官方文档：
[Java8官方文档](https://docs.oracle.com/javase/8/docs/api/)
# Java语法
Java语法是Java学习体系中最基础的一节，也是必须掌握的一节。读者可以采取四种方式学习Java语法部分。
1. 通过笔记学习，绝大多数的理论知识都写在笔记中，如果有哪些地方不懂，翻看笔记对应的章节即可。
2. 通过本仓库提供的代码学习，代码依附于笔记，是完全按照笔记的逻辑整理的。
3. 通过思维导图学习，有助于读者构建知识体系，梳理知识脉络。拓宽知识学习的宽度。
4. 通过博客学习，博客针对某个知识点会有较为全面的讲解，提高读者学习的深度。下面会提供文章连接，直接点击学习即可。
## 介绍
介绍Java语法及代码实现
## 仓库架构
 - Java基础
     - Exception（异常）
 - Java面向对象
     - 类和对象
     - 继承
     - 重写&重载
     - 多态
     - 抽象和接口
     - 枚举类&注解
 - Java高级
     - 多线程
     - Java集合框架
     - 泛型
     - IO流
     - 网络编程
     - Java反射机制
## 文章连接
 - [接口和抽象类的区别](https://blog.csdn.net/weixin_62633072/article/details/124124363)
 - [Java中的内存分配](https://blog.csdn.net/weixin_62633072/article/details/124534452)
 - [今天终于会写System.out.println()了](https://blog.csdn.net/weixin_62633072/article/details/124347816)
 - [【面试题】HashMap底层原理？equals、hashCode全部重写？](https://blog.csdn.net/weixin_62633072/article/details/124294498)
 - 

